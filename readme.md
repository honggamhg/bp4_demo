# Backpack
Backpack là tập hợp các package giúp xây dựng trang quản lý.
Xây dựng giao diện sẵn theo mẫu, CRUD theo mô hình Eloquent các bảng quản trị nhanh chóng
## UI
Sử dụng khung HTML xây dựng giao diện sẵn theo mẫu [BackStrap]( https://www.npmjs.com/package/@digitallyhappy/backstrap) 
các thành phần giao diện được xây dựng và có thể sửa đổi (file tương ứng trong vendor) trong `resources/views/vendor/backpack/base/`
* Backpack cung cấp một số trang lỗi HTTP cơ bản như các mã: 400, 401, 403, 404, 405, 408, 429, 500, 503. Ngoài ra người dùng có thể tự định nghĩa các trang lỗi khác trong `resources/views/errors`
* Widget
Là các thành phần bổ sung vào trang quản trị như noti, chart, card, hoặc người dùng có thể tự thêm các nội dung. Backpack cung cấp 1 số default widget: Alert, card, div, Jumbotron, ... Có thể chỉnh sửa các default widget hoặc tự tạo các widget trong `resources\views\vendor\backpack\base\widgets`' 
## Các package:

#### 1. CRUD 
Là package cơ bản của backpack, install ngay từ đầu, quản lý CRUD cho từng model
Tạo bảng quản trị cho 1 Model use CrudTrait có sẵn (hoặc tạo migrate): `php artisan backpack:crud [model_name]` khi đó sẽ tạo ra: 
  *	`CrudController` trong `app\Http\Controllers\Admin`
  *	`CrudRequest` trong `app\Http\Request`
  *	Và route tương tứng với [name_model] trong `routes/backpack/custom.php` . Sau đó có thể đưa item đó ra sidebar.
* Các hành động CRUD được thiết lập trong controller kế thừa từ CrudController
###### Các Operations CRUD ( `\Backpack\CRUD\app\Http\Controllers\Operations`) tương ứng cấu hình trong các phương thức:
* `ListOperation` – `setupListOperation`: Hiển thị thông tin ra bảng, có thể custom bằng cách ghi đè lên phương thức showDetailsRow trong controller.
*	`CreateOperatio` – `setupCreateOperation` (ghi đè vào `store()` ): tạo mới một bản ghi, có thể custom các column trong phương thức trên. 
*	`UpdateOperation` – `setupUpdateOperation` (`update()`) : Chỉnh sửa bản ghi và update.
*	`DeleteOperation` – `setupDeleteOperation` (`destroy()`): Xoá bản ghi
*	`ShowOperation` – `setupShowOperation` (`show()`): xem 1 mục chi tiết trong bảng.
###### *Ngoài ra còn có các operation khác (ít dùng hơn)*: 
*	BulkCloneOperation: Sao chép nhiều mục
*	BulkDeleteOperation (bulkDelete()):  Xoá nhiều mục
*	CloneOperation: (clone()) Sao chép 1 bản ghi
*	ReorderOperation: Sắp xếp thứ tự các bản ghi, hoặc quan hệ cha con nếu có
*	RevisionsOperation: Lưu lại các lịch sử edit và có thể xem và hoàn tác lại
###### Một số thành phần trên list cần chú ý:
*	Column: Là các cột, có thể chỉ định hiển thị trên bảng quản trị và thứ tự của chúng. Có thể `addColumn/removeColumn/setColumDetails` với 14 loại default column như: text, number, array, boolean, check/checkbox, date, image, markdown,… Có thể ghi đè lên các default column bằng cách custom file [ten_column] trong `resources\views\vendor\backpack\crud\columns` Và tạo mới column tuỳ người dùng cũng trong thư mục trên.
*	Fields: Các trường input trong các chức năng create/edit, có thể chỉ định tạo và sắp thứ tự tương tự như column. Có nhiều loại fields default: image, text, date, password, checkbox, checklist, select, address, table, range, color, … cũng có thể ghi đè hoặc tự tạo ra các fields trong `resources\views\vendor\backpack\crud\fields`
*	Filter: Bộ lọc hiển thị trên bảng quản trị, khi lọc theo 1 đk nào đó, bảng sẽ load lại và hiển thị list mới theo các yếu tố được lọc. Các thao tác tương tự như với column, có 8 loại default filter: simple, text, date, date range, dropdown, select2, select2_multiple, select2_ajax, range, view. Ngoài ra cũng có thể tự custom filter trong  `resources/views/vendor/backpack/crud/filters`.
*	Button: sử dụng để thực hiện các hanh động mặc định (operation) như: Show, Delete, Save, Edit, Add, … hoặc các chức năng người dùng custom. 3 vị trí với button : top (nút Add), line (Edit, View, Delete), bottom (Save). Ngoài ra có thể ghi đè các nút mặc định như trên hoặc tự tạo các button với các chức năng khác trong `resources\views\vendor\backpack\crud\buttons`.


#### 2. Base & Generators
Từ v4, backpack\base vaf backpack\generators đc bao gồm trong backpack\crud xây dựng trang quản trị các thành phần như: 
* Base - giao diện login, basic menu, alerts system, trang lỗi. 
* Generators - tạo nhanh các Models, Requests, Views, Config files
#### 3. PermissionManager
Xây dựng trang quản trị User/Role/Permission, CRUD user tương ứng vai trò, 1 người dùng có thể có nhiều vai trò, tương ứng với nhiều quyền.
###### Role
* gán role cho user: `backpack_user()->assignRole('role_name')`
* gỡ role:  `backpack_user()->removeRole('role_name')`
* kiểm tra `backpack_user()->hasRole('role_name')`
* kiểm tra xem user có được gán vai trò nào ko `backpack_user()->hasAnyRole(Role::all())`
* hiển thị list role user có `backpack_user()->hasAllRoles(Role::all())`
###### Permission
* cấp quyển: `backpack_user()->givePermissionTo('permission_name')`
* xoá bỏ quyền `backpack_user()->revokePermissionTo('permission_name')`
* kiểm tra `backpack_user()->hasPermissionTo('permission_name')`
###### `role->perrmission`
* gán quyền cho role `$role->givePermissionTo('permission_name')`
* kiểm tra role có quyền hay không `$role->hasPermissionTo('permission_name')`
* gỡ quyền `$role->revokePermissionTo('permission_name')`

#### 4. PageManager
Tạo 1 trang và quản lý các page 1 cách nhanh chóng
Sử dụng các mẫu template hoặc tự custom theo ý muốn trong `app/PageTemplate.php`. Sau đó tạo route và Controller của page tương ứng
#### 5. NewCRUD
Hỗ trợ CRUD liên quan đến New, các bài viết theo danh mục và gắn tag: 
* Article
* Category
* Tag
#### 6. MenuCRUD
CRUD các mục menu, có thể liên kết đến 1 page trong backpack/pagemanager
#### 7. BackupManager
Cung cấp trang quản lý các lần backup dữ liệu, xem hoặc xoá. 
#### 8. LogManager
Cung cấp giao diện trực quan để xem, xoá hoặc tải xuống log dễ dàng.


