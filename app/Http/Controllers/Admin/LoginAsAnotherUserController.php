<?php
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 2019-10-08
 * Time: 12:30
 */

namespace App\Http\Controllers\Admin;


use App\User;

class LoginAsAnotherUserController {

    public function __invoke($id) {

        if($id == 0 && \Session::has( 'previous_logedin_user')){
            $previous_user = User::findOrFail( \Session::get( 'previous_logedin_user') );
            \Session::forget( 'previous_logedin_user');
            backpack_auth()->login( $previous_user );
            return redirect(backpack_url());
        }

        $current_user = backpack_user();
        $another_user = User::findOrFail( $id );
        if($current_user->can('loginAs', $another_user)){
            \Session::put( 'previous_logedin_user', $current_user->id);
            backpack_auth()->login( $another_user );
            return redirect(backpack_url());
        }else{
            abort( 403 );
        }
    }

}
