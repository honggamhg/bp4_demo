<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InformationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class InformationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class InformationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Information');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/information');
        $this->crud->setEntityNameStrings('information', 'information');

        $this->crud->enableExportButtons();

        $this->crud->addColumn([
            'name' => 'email',
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInExport' => false,
            'visibleInShow' => false,
        ]);

    }

    protected function setupListOperation()
    {
        $this->crud->addFilter([
            'type' => 'simple',
            'name' => 'email',
            'label'=> 'Email'
        ],
            false, // the simple filter has no values, just the "Draft" label specified above
            function() { // if the filter is active (the GET parameter "draft" exits)
                $this->crud->addClause('where', 'id', '1');
            });
        $this->crud->addFilter([
            'type' => 'text',
            'name' => 'title',
            'label'=> 'Title'
        ],
            false,
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'title', 'LIKE', "%$value%");
            });
        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'date',
            'label' => 'Date range'
        ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'date', '>=', $dates->from. '00:00:00');
                $this->crud->addClause('where', 'date', '<=', $dates->to . ' 23:59:59');
            });
        $this->crud->addFilter([
            'name'  => 'status',
            'type'  => 'select2',
            'label' => 'Status'
        ], function () {
            return [
                'PUBLISHED' => 'PUBLISHED',
                'DRAFT' => 'DRAFT',
            ];
        }, function ($value) {
            $this->crud->addClause('where', 'status', $value);
        });

        $this->crud->addFilter([
            'name' => 'category_id',
            'type' => 'select2',
            'label'=> 'Category'
        ], function() {
            return \App\Models\Category::all()->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'category_id', $value);
        });


//Column
        $this->crud->addColumn('email');

//        $this->crud->addColumn([
//            'name' => 'options',
//            'label' => 'Options',
//            'type' => 'table',
//            'columns' => [
//                'name' => 'Name',
//                'price' => 'Price',
//            ]
//            ]);
//        $this->crud->addColumn([
//            'name' => 'address',
//            'label' => 'Address',
//            //'type' => 'array',
//        ]);
        $this->crud->addColumn([
            'label' => "Profile Image",
            'name' => "image",
            'type' => 'image',
            'prefix' => 'http://127.0.0.1:8000/',
            'height' => '50px',
            'width' => '50px',
        ]);
        $this->crud->addColumn([
            'name' => 'video', // The db column name
            'label' => "Video", // Table column heading
            'type' => 'video',
        ]);
        $this->crud->addColumn('status');
        $this->crud->addColumn([
            'name' => 'email',
            'label' => "Email Address",
            'type' => 'email',
        ]);
//        $this->crud->addColumn([
//            'label' => 'Category',
//            'type' => 'select',
//            'name' => 'category_id',
//            'entity' => 'category',
//            'attribute' => 'name',
//        ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(InformationRequest::class);


        $this->crud->addField([
            'label' => "Profile Image",
            'name' => "image",
            'type' => 'image',
            'upload' => true,
            'tab' => 'Upload'
        ]);

//        $this->crud->addField([
//            'name' => 'custom-ajax-button',
//            'type' => 'view',
//            'view' => 'custom_view',
//            'tab' => 'Time',
//        ]);


        $this->crud->addField([
            'name' => 'address',
            'label' => 'Address',
            'type' => 'address_algolia',
            'store_as_json' => true,
            'tab' => 'Time',
        ]);
//        $this->crud->addField([
//            'name' => 'separator',
//            'type' => 'custom_html',
//            'value' => '<img src="http://127.0.0.1:8000/uploads/folder_1/folder_2/52dbd7cff173fe0793a52d2461cb39b5.jpg?_t=1571630025" width="100px" height="100px"></img>',
//'
//        ]);

//BAse64
//        $this->crud->addField([
//            'label' => "Profile Image",
//            'name' => "image",
//            'filename' => "image_filename", // set to null if not needed
//            'type' => 'base64_image',
//            'aspect_ratio' => 1, // set to 0 to allow any aspect ratio
//            'crop' => true, // set to true to allow cropping, false to disable
//            'src' => NULL, // null to read straight from DB, otherwise set to model accessor function
//            'store_as_json' => true,
//            'tab' => 'Upload',
//        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => 'Description',
            'type' => 'ckeditor',
            'tab' => 'Text',
            // optional:
            'extra_plugins' => ['oembed', 'widget'],
            'options' => [
                'autoGrow_minHeight' => 200,
                    'autoGrow_bottomSpace' => 50,
                'removePlugins' => 'resize,maximize',
            ]
        ]);

        $this->crud->addField([
            'label' => 'Background Color',
            'name' => 'background_color',
            'type' => 'color_picker',
            'default' => '#000000',
            'tab' => 'Other',
            'color_picker_options' => ['customClass' => 'custom-class']
         ]);
        $this->crud->addField([
            'name' => ['start_date', 'end_date'], // db columns for start_date & end_date
            'label' => 'Event Date Range',
            'type' => 'date_range',
            'tab' => 'Time',
            // OPTIONALS
            'default' => ['2019-03-28 01:01', '2019-04-05 02:00'], // default values for start_date & end_date
            'date_range_options' => [ // options sent to daterangepicker.js
                'timePicker' => true,
                'locale' => ['format' => 'DD/MM/YYYY HH:mm']
            ]
        ]);
        $this->crud->addField([
            'name' => 'start',
            'label' => 'Event start',
            'type' => 'datetime_picker',
            // optional:
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'fr'
            ],
            'tab' => 'Time',
            'allows_null' => true,
        ]);
        $this->crud->addField([
            'name' => 'email',
            'label' => 'Email Address',
            'type' => 'email',
            'tab' => 'Text',
        ]);
        $this->crud->addField([
            'name'        => 'status_2',
            'label'       => 'Status radio',
            'type'        => 'radio',
            'options'     => [
                0 => "Draft",
                1 => "Published"
            ],
            'tab' => 'Select',
        ]);
        $this->crud->addField([
            'label' => "Icon",
            'name' => 'icon',
            'type' => 'icon_picker',
            'iconset' => 'fontawesome',
            'store_as_json' => true,
            'tab' => 'Icon',
        ]);
        $this->crud->addField([
            'name' => 'month',
            'label' => 'Month',
            'type' => 'month',
            'tab' => 'Time',
        ]);
        $this->crud->addField([
            'name' => 'type',
            'label' => "Type",
            'type' => 'page_or_link',
            'page_model' => '\Backpack\PageManager\app\Models\Page',
            'store_as_json' => true,
            'tab' => 'Other',
        ]);
        $this->crud->addField([
            'name' => 'password',
            'label' => 'Password',
            'type' => 'password',
            'tab' => 'Text',
        ]);
//        $this->crud->addField([
//            'name' => 'password',
//            'label' => 'Password',
//            'type' => 'select_page_template',
//            'tab' => 'Text',
//        ]);

        $this->crud->addField([
            'name' => 'range',
            'label' => 'Range',
            'type' => 'range',
            'tab' => 'Other',
            //'store_as_json' => true,
        ]);
        $this->crud->addField([
            'name' => 'description_2',
            'label' => 'Description',
            'type' => 'summernote',
            'tab' => 'Text',
        ]);
        $this->crud->addField([
            'name' => 'options',
            'label' => 'Options',
            'type' => 'table',
            'entity_singular' => 'option', // used on the "Add X" button
            'columns' => [
                'name' => 'Name',
                'price' => 'Price'
            ],
            'tab' => 'Select',
            'max' => 5, // maximum rows allowed in the table
            'min' => 0, // minimum rows allowed in the table
        ]);
        $this->crud->addField([
            'name' => 'photos',
            'label' => 'Upload file',
            'type' => 'upload_multiple',
            'upload' => true,
            'temporary' => 10,
            'store_as_json' => true,
            'tab' => 'Upload',
        ]);
        $this->crud->addField([
            'name' => 'video',
            'label' => 'Link to video file on Youtube or Vimeo',
            'type' => 'video',
            'store_as_json' => true,
            'tab' => 'Upload',
        ]);
//        $this->crud->addField([
//            'name' => 'separator',
//            'type' => 'custom_html',
//            'value' => '<hr><h1>Hi</h1> <hr>',
//            'store_as_json' => true,
//            'tab' => '',
//        ]);
        $this->crud->addField([
            'label' => 'Category',
            'type' => 'select2',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'tab' => 'Select',
        ]);
        $this->crud->addField([
            'label' => "Roles - Select_multiple",
            'type' => 'select2_multiple',
            'name' => 'roles', // the method that defines the relationship in your Model
            'entity' => 'roles', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            //'model' => "App\Models\Tag", // foreign key model
            'pivot' => true,
            'tab' => 'Select',
        ]);

//        $this->crud->addField([
//            'label' => "select2_from_ajax_multiple", // Table column heading
//            'type' => "select2_from_ajax_multiple",
//            'name' => 'category_id', // the column that contains the ID of that connected entity
//            'entity' => 'category', // the method that defines the relationship in your Model
//            'attribute' => "name", // foreign key attribute that is shown to user
//            'model' => "App\Models\Category", // foreign key model
//            'data_source' => url("api/category"), // url to controller search function (with /{id} should return model)
//            'placeholder' => "Select a category", // placeholder for the select
//            'minimum_input_length' => 1,
//            'tab' => 'Select',
//        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


}
