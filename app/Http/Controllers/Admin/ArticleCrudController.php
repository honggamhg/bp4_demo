<?php

namespace App\Http\Controllers\Admin;

use App\PageTemplates;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\ArticleRequest;
use Backpack\PageManager\app\Http\Requests\PageRequest;

class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use PageTemplates;

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Article");
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin') . '/article');
        $this->crud->setEntityNameStrings('article', 'articles');

        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();

        $this->crud->addColumn([
            'name' => 'title',
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInExport' => false,
            'visibleInShow' => false,
            //'searchLogic' => false,
        ]);

        /*
        |--------------------------------------------------------------------------
        | LIST OPERATION
        |--------------------------------------------------------------------------
        */
        $this->crud->operation('list', function () {

            $this->crud->addFilter([
                'type' => 'simple',
                'name' => 'draft',
                'label'=> 'Draft'
            ],
                false, // the simple filter has no values, just the "Draft" label specified above
                function() { // if the filter is active (the GET parameter "draft" exits)
                    $this->crud->addClause('where', 'draft', '1');
                });
            $this->crud->addFilter([
                'type' => 'text',
                'name' => 'title',
                'label'=> 'Title'
            ],
                false,
                function($value) { // if the filter is active
                     $this->crud->addClause('where', 'title', 'LIKE', "%$value%");
                });
            $this->crud->addFilter([
                'type'  => 'date_range',
                'name'  => 'date',
                'label' => 'Date range'
            ],
                false,
                function ($value) { // if the filter is active, apply these constraints
                     $dates = json_decode($value);
                     $this->crud->addClause('where', 'date', '>=', $dates->from. '00:00:00');
                     $this->crud->addClause('where', 'date', '<=', $dates->to . ' 23:59:59');
                });
            $this->crud->addFilter([
                'name'  => 'status',
                'type'  => 'select2',
                'label' => 'Status'
            ], function () {
                return [
                    'PUBLISHED' => 'PUBLISHED',
                    'DRAFT' => 'DRAFT',
                ];
            }, function ($value) {
                $this->crud->addClause('where', 'status', $value);
            });

            $this->crud->addFilter([
                'name' => 'category_id',
                'type' => 'select2',
                'label'=> 'Category'
            ], function() {
                return \App\Models\Category::all()->pluck('name', 'id')->toArray();
            }, function($value) { // if the filter is active
                $this->crud->addClause('where', 'category_id', $value);
            });



            $this->crud->addColumn('title');
            $this->crud->addColumn([
                'name' => 'template',
                'label' => trans('backpack::pagemanager.template'),
            ]);
            $this->crud->addColumn([
                'name' => 'date',
                'label' => 'Date',
                'type' => 'date',
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => false,
            ]);
            $this->crud->addColumn('status');
            $this->crud->addColumn([
                'name' => 'featured',
                'label' => 'Featured',
                'type' => 'check',
            ]);
            $this->crud->addColumn([
                'label' => 'Category',
                'type' => 'select',
                'name' => 'category_id',
                'entity' => 'category',
                'attribute' => 'name',
            ]);
        });


        /*
        |--------------------------------------------------------------------------
        | CREATE & UPDATE OPERATIONS
        |--------------------------------------------------------------------------
        */
        $this->crud->operation(['create', 'update'], function () {
            $templates = $this->getTemplates();
            $template = (\Request::input('template'))?(\Request::input('template')):$templates[0]->name;

            $this->addDefaultPageFields($template);
            $this->useTemplate(\Request::input('template'));
            $this->crud->setValidation(ArticleRequest::class);
        });

        $this->crud->operation('update', function () {
            $template = \Request::input('template') ?? $this->crud->getCurrentEntry()->template;

            $this->addDefaultPageFields($template);
            $this->useTemplate($template);

            $this->crud->setValidation(PageRequest::class);
        });
    }

    public function addDefaultPageFields($template = false)
    {
        $this->crud->addField([
            'name' => 'template',
            'label' => trans('backpack::pagemanager.template'),
            'type' => 'select_page_template',
            'view_namespace'  => 'pagemanager::fields',
            'options' => $this->getTemplatesArray(),
            'value' => $template,
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'type' => 'text',
            'placeholder' => 'Your title here',
        ]);
        $this->crud->addField([
            'label' => 'Category',
            'type' => 'select2',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your title, if left empty.',
            // 'disabled' => 'disabled'
        ]);
    }

    public function useTemplate($template_name = false)
    {
        $templates = $this->getTemplates();
        // set the default template
        if ($template_name == false) {
            $template_name = $templates[0]->name;
        }

        // actually use the template
        if ($template_name) {
            $this->{$template_name}();
        }
    }

    public function getTemplates($template_name = false)
    {
        $templates_trait = new \ReflectionClass('App\PageTemplates');
        $templates = $templates_trait->getMethods(\ReflectionMethod::IS_PRIVATE);
        if (! count($templates)) {
            abort(503, trans('backpack::pagemanager.template_not_found'));
        }

        return $templates;
    }

    public function getTemplatesArray()
    {
        $templates = $this->getTemplates();

        foreach ($templates as $template) {
            $templates_array[$template->name] = str_replace('_', ' ', title_case($template->name));
        }

        return $templates_array;
    }
}
