<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DeleteRequest;
use App\Http\Requests\TagRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Models\Tag;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TagCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TagCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation { destroy as traitDestroy; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\RevisionsOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Tag');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/tag');
        $this->crud->setEntityNameStrings('tag', 'tags');
        $this->crud->denyAccess('delete');
        if(backpack_user()->hasRole('Admin')){
            $this->crud->allowAccess('delete');
        }
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'slug_or_title',
            'label' => 'Title',
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('title', 'like', '%'.$searchTerm.'%');
            }
        ]);
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TagRequest::class);


        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setValidation(UpdateTagRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Tag name'
        ]);

//        $this->setupCreateOperation();
    }

    public function destroy($id)
    {
        if( (backpack_user()->can('delete', Tag::class)) && ($this->request->id != backpack_user()->id)){
            $this->crud->hasAccessOrFail('delete');
            return $this->crud->delete($id);
        }
    }
}
