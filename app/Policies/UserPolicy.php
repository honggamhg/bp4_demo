<?php

namespace App\Policies;

use App\User;
use App\Models\BackpackUser;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Contracts\Auth\Authenticatable;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function delete(BackpackUser $user)
    {
        return $user->hasRole('Admin');
    }

    public function loginAs(Authenticatable $logedInUser, Authenticatable $anotherUser){
        if($logedInUser->hasRole('Admin')){
            return true;
        }
    }

}
