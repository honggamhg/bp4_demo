<?php

namespace App\Policies;

use App\Models\BackpackUser;
use App\Models\Tag;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TagPolicy
{
    use HandlesAuthorization;


    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update( BackpackUser $user)
    {
        return $user->hasRole('Admin');
    }

    public function delete(BackpackUser $user)
    {
        return $user->hasRole('Admin');
    }
}
