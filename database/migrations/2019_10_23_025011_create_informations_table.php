<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('select_2');
            $table->json('address');
            $table->string('image');
            $table->string('roles');
            $table->string('description');
            $table->string('background_color');
            $table->date('start_date');
            $table->date('end_date');
            $table->dateTime('start');
            $table->string('email');
            $table->enum('status', ['PUBLISHED', 'DRAFT'])->default('PUBLISHED');
            $table->string('icon');
            $table->string('image_2');
            $table->date('month'); //??
            $table->string('type');
            $table->string('password');
            $table->string('status_2');
            $table->string('range');
            $table->text('description_2');
            $table->json('options');
            $table->string('photos');
            $table->string('video');
            $table->string('separator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Informations');
    }

}
