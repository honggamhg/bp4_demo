<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('tag', 'TagCrudController');
    Route::crud('role', 'RoleCrudController');
    Route::crud('user', 'UserCrudController');
    Route::get( 'login_as/{id}', ['as' => 'admin.login_as', 'uses' => 'LoginAsAnotherUserController']);

    Route::crud('permission', 'PermissionCrudController');

    Route::crud('article', 'ArticleCrudController');
    Route::crud('category', 'CategoryCrudController');

    Route::crud('information', 'InformationCrudController');


    Route::get('/api/category', 'Api\CategoryController@index');
    Route::get('/api/category/{id}', 'Api\CategoryController@show');
}); // this should be the absolute last line of this file
