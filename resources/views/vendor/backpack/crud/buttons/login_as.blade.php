<a href="{{ route('admin.login_as', ['id' => $entry->getKey()]) }}" class="btn btn-sm btn-link" title="Login as">
  <span class="fa-stack">
    <i class="fa fa-users fa-stack-1x"></i>
    <i class="fa fa-refresh fa-stack-2x"></i>
  </span>
  <span class="sr-only">{{ __('Login as') }}</span>
</a>