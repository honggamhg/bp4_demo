<div class="btn-group">
  <a href="javascript:void(0);" class="btn btn-sm btn-link dropdown-toggle text-danger pl-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-gear"></i>
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu dropdown-menu-right">
    <li class="dropdown-header">Actions :</li>
    <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/view_rules') }}">View rules</a>
    <a class="dropdown-item" href="#">View stack</a>
    <a class="dropdown-item" href="#">Run test</a>
    <a class="dropdown-item text-danger" href="#">Reset</a>
  </ul>
</div>