<input type="file" id="site_import" style="display: none" />
<button class="btn btn-xs btn-warning" id="site_import_button" > <i class="fa fa-upload"></i> {{__('Import')}}</button>

@push('after_scripts')
  <script type="application/javascript">
      jQuery('#site_import_button').click(function (e) {
          jQuery('#site_import').trigger('click');
      });
      jQuery('#site_import').on('change', function (e) {

          var fileReader = new FileReader();
          fileReader.onload = function (e) {
              jQuery.post('{{route('sites.import')}}', { site_info : JSON.parse(e.target.result) }, function(res){
                  if(res.success){
                      new Noty({
                          type: "success",
                          text: "<strong>{{ trans('backpack::crud.bulk_delete_sucess_title') }}</strong><br>" + res.message
                      }).show();
                      crud.table.ajax.reload();
                  }else{
                      new Noty({
                          type: "warning",
                          text: "<strong>{{ trans('backpack::crud.bulk_delete_error_title') }}</strong><br>" + res.message
                      }).show();
                  }
              });
          };
          fileReader.readAsText($('#site_import').prop('files')[0]);
      });
  </script>
  @endpush
